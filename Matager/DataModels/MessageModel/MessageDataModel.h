//
//  MessageDataModel.h
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MessageDataModel : NSObject
{
    NSString * _messageID;
    NSString * _messageTitle;
    NSString * _messageContent;
    NSString * _messageSender;
    NSString * _messageSenderID;
    BOOL _messageTypeInbox;
    NSString * _messageTime;
}

@property (nonatomic ,copy) NSString* messageID;
@property (nonatomic ,copy) NSString* messageTitle;
@property (nonatomic ,copy) NSString* messageContent;
@property (nonatomic ,copy) NSString* messageSender;
@property (nonatomic ,copy) NSString* messageSenderID;
@property (nonatomic ) BOOL messageTypeInbox;
@property (nonatomic ,copy) NSString* messageTime;



-(MessageDataModel *) init;
-(MessageDataModel *) initWithDictionary:(NSDictionary *)messageDictionary forInbox:(BOOL)isInbox;


@end
