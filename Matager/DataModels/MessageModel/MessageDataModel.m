//
//  MessageDataModel.m
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MessageDataModel.h"

@implementation MessageDataModel
@synthesize messageContent=_messageContent;
@synthesize messageID=_messageID;
@synthesize messageSender=_messageSender;
@synthesize messageTime=_messageTime;
@synthesize messageTitle=_messageTitle;
@synthesize messageTypeInbox=_messageTypeInbox;
@synthesize messageSenderID=_messageSenderID;

- (MessageDataModel *)init
{
    self = [super init];
    if (self) {
        _messageID=@"-1";
        _messageTitle =@"غير معرف";
        _messageContent=@"";
        _messageSender=@"غير معرف";
        _messageTime=@"";
    }
    return self;
}

-(MessageDataModel *)initWithDictionary:(NSDictionary *)messageDictionary forInbox:(BOOL)isInbox{
    self = [super init];
    if (self) {
        _messageID=[messageDictionary objectForKey:@""];
        _messageTitle =[messageDictionary objectForKey:@""];
        _messageContent=[messageDictionary objectForKey:@""];
        _messageSender=[messageDictionary objectForKey:@""];
        _messageSenderID=[messageDictionary objectForKey:@""];
        _messageTime=[messageDictionary objectForKey:@""];
        _messageTypeInbox = isInbox;
    }
    return self;
}

@end
