//
//  ProductModel.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

enum homeProductType {
    newProducts =0 ,
    noPicProducts = 1,
    cityProducts=2
};

#import <Foundation/Foundation.h>

@interface ProductModel : NSObject

@property (nonatomic , retain) NSString * productID;
@property (nonatomic , retain) NSString * productName;

@property (nonatomic , retain) NSString * productDescription;

@property (nonatomic , retain) NSString *productOwnerName;
@property (nonatomic , retain) NSString *productOwnerID;

@property (nonatomic , retain) NSString *productCountry;
@property (nonatomic , retain) NSString *productCounrtyID;

@property (nonatomic , retain) NSString *productPostDate;
@property (nonatomic , retain) NSString *productType;

@property (nonatomic , retain) NSString *productImageURL;
@property (nonatomic , retain) NSMutableArray *productImagesArray;

@property (nonatomic , retain)NSString * productCategory;

@property (nonatomic ) BOOL isDeleted;

@property (nonatomic , retain) NSMutableArray *productComments;

@end
