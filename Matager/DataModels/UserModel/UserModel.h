//
//  UserModel.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic) BOOL islogged;

@property (nonatomic ,retain) NSString * userID;
@property (nonatomic ,retain) NSString * userName;

@property (nonatomic ,retain) NSString * userPassword;

@property (nonatomic ,retain) NSMutableArray * userAds;

-(id)initLoggedUserWithUserID:(NSString *)ID withName:(NSString *)name andPassword:(NSString *)password;

-(NSDictionary *)userModelLoginDictionary;

@end
