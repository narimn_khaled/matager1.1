//
//  UserModel.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel
@synthesize userAds , userID , userName , userPassword , islogged ;

- (id)init
{
    self = [super init];
    if (self) {
        userID=@"";
        userName=@"";
        userAds =[NSMutableArray new];
        islogged =NO;
    }
    return self;
}

-(id)initLoggedUserWithUserID:(NSString *)ID withName:(NSString *)name andPassword:(NSString *)password{
    self = [super init];
    if (self) {
        userID=ID;
        userName=name;
        userPassword=password;
        islogged=YES;
    }
    return self;
}

-(NSDictionary *)userModelLoginDictionary:(UserModel *)user{
   return [[NSDictionary alloc]initWithObjectsAndKeys:userName ,@"name", userPassword ,@"pass", nil];
}

@end
