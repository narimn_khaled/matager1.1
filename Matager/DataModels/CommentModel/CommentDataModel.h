//
//  CommentDataModel.h
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentDataModel : NSObject


@property (nonatomic ,copy) NSString* commentID;
@property (nonatomic ,copy) NSString* commentTitle;
@property (nonatomic ,copy) NSString* commentContent;
@property (nonatomic ,copy) NSString* commentSender;
@property (nonatomic ,copy) NSString* commentSenderID;
@property (nonatomic ,copy) NSString* commentTime;

@property (nonatomic ) BOOL commentHaveChild;
@property (nonatomic ,copy) NSString* childCommentTime;
@property (nonatomic ,copy) NSString* childCommentContent;


-(CommentDataModel*)init;
-(CommentDataModel *)initWithDictionary:(NSDictionary *)commentDictionary;

@end
