//
//  CommentDataModel.m
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "CommentDataModel.h"

@implementation CommentDataModel

@synthesize commentContent=_commentContent;
@synthesize commentHaveChild=_commentHaveChild;
@synthesize commentID=_commentID;
@synthesize commentSender=_commentSender;
@synthesize commentSenderID=_commentSenderID;
@synthesize commentTime=_commentTime;
@synthesize commentTitle=_commentTitle;
@synthesize childCommentContent=_childCommentContent;
@synthesize childCommentTime=_childCommentTime;



- (id)init
{
    self = [super init];
    if (self) {
        _commentTitle =@"";
        _commentTime =@"";
        _commentSender =@"";
        _commentSenderID=@"";
        _commentHaveChild=NO;
        
    }
    return self;
}


-(CommentDataModel *)initWithDictionary:(NSDictionary *)commentDictionary{
    self = [super init];
    if (self) {
        _commentTitle =[commentDictionary objectForKey:@""];
        _commentTime =[commentDictionary objectForKey:@""];
        _commentSender =[commentDictionary objectForKey:@""];
        _commentSenderID=[commentDictionary objectForKey:@""];
        _commentHaveChild=[[commentDictionary objectForKey:@""] isEqualToString:@"1"]?YES:NO;
        if (_commentHaveChild) {
            _childCommentTime=[commentDictionary objectForKey:@""];
            _childCommentContent=[commentDictionary objectForKey:@""];
        }
        
    }
    return self;
}

@end
