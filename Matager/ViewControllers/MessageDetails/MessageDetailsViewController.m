//
//  MessageDetailsViewController.m
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MessageDetailsViewController.h"
#import "SendMessagePopupViewController.h"

@interface MessageDetailsViewController ()

@end

@implementation MessageDetailsViewController
@synthesize message;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setPageTitle:message.messageTypeInbox ? @"الرسائل المستلمة":@"الرسائل المرسلة"];
    [self.replyBtn setHidden:   !message.messageTypeInbox];
    [messageContent setText:message.messageContent];
    [messageSender setText:message.messageSender];
    [messageTime setText:message.messageTime];
    [messageTitle setText:message.messageTitle];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)headerBackButtonTouched:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    SendMessagePopupViewController * sendMsg=segue.destinationViewController;
    [sendMsg setMessageToReplyTo:self.message];
}

@end
