//
//  MessageDetailsViewController.h
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"
#import "MessageDataModel.h"

@interface MessageDetailsViewController : MasterViewController
{
    __weak IBOutlet UILabel *messageSender;
    __weak IBOutlet UILabel *messageTime;
    __weak IBOutlet UILabel *messageTitle;
    __weak IBOutlet UITextView *messageContent;
    
}
@property (nonatomic , retain) MessageDataModel * message;

@property (weak, nonatomic) IBOutlet UIButton *replyBtn;
- (IBAction)replyButtonTouched:(UIButton *)sender;
@end
