//
//  MessagesCell.h
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageDataModel.h"
#import "ZKRevealingTableViewCell.h"
@interface MessagesCell : ZKRevealingTableViewCell
{
    __weak IBOutlet UIImageView *messageCellBg;
    
    __weak IBOutlet UILabel *messageTitle;
    __weak IBOutlet UILabel *messageSender;
    __weak IBOutlet UILabel *messageTime;
}

@property (weak, nonatomic) IBOutlet UIButton *messageSenderButton;
-(void)setCellMessageDataWithMessage:(MessageDataModel *)message forIndex:(int)index;

@end
