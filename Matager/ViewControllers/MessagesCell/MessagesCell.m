//
//  MessagesCell.m
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MessagesCell.h"
#import "colors.h"

@implementation MessagesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        [messageCellBg setImage:[UIImage imageNamed:@"msg_item_hover"]];
    }else{
        [self setCellBgFromCellIndex];
    }
}


-(void)setCellMessageDataWithMessage:(MessageDataModel *)message forIndex:(int)index{
    [messageSender setTextColor:UIColorFromRGBF(messageGrayColor)];
    [messageTime setTextColor:UIColorFromRGBF(messageGrayColor)];
    [messageTitle setTextColor:UIColorFromRGBF(messageBlackColor)];
    
    [self setTag:index];// set the tag as the index to user it to get bg.
    [self setCellBgFromCellIndex];
    
    [messageSender setText:message.messageSender];
    [messageTime setText:message.messageTime];
    [messageTitle setText:message.messageTitle];
    
    [self.messageSenderButton setTag:[message.messageSenderID integerValue]];
}

-(void)setCellBgFromCellIndex{
    
    if ([self tag]%2) {
        [messageCellBg setImage: [UIImage imageNamed:@"msg_item_white"]];
    }else{
        [messageCellBg setImage:[UIImage imageNamed:@"msg_item_gray"]];
    }
    
}

@end
