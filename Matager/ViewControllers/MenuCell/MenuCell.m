//
//  MenuCell.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MenuCell.h"
#import "colors.h"

@implementation MenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        [self.menuCellImgBtn setSelected:YES];
        [self.menuCellTitle setTextColor:[UIColor UIColorFromRGB(sideMenuItemColor_s)]];
    }else{
        [self.menuCellImgBtn setSelected:NO];
        [self.menuCellTitle setTextColor:[UIColor UIColorFromRGB(sideMenuItemColor)]];

    }
    
}
-(void)setCellWithTitle:(NSString *)title andImage:(NSString *)imgName{
    
}

-(void)setCellWithTitle:(NSString *)title {
    [self.menuCellTitle setText:title];
    [self.menuCellImgBtn setImage:[UIImage imageNamed:@"m_general_s0"] forState:UIControlStateNormal];
     [self.menuCellImgBtn setImage:[UIImage imageNamed:@"m_general_s1"] forState:UIControlStateSelected];
    
}

-(void)setCellWithDictionary:(NSDictionary *)dictionary{
    [self.menuCellTitle setText:[dictionary objectForKey:@"title"]];
    [self.menuCellImgBtn setImage:[UIImage imageNamed:[dictionary objectForKey:@"img"]] forState:UIControlStateNormal];
    [self.menuCellImgBtn setImage:[UIImage imageNamed:[dictionary objectForKey:@"img_s"]] forState:UIControlStateSelected];
}

-(void)setCellWithSearchTitle:(NSString *) title{
    [self.menuCellTitle setText:title];
    [self.menuCellImgBtn setImage:[UIImage imageNamed:@"m_search_s0"] forState:UIControlStateNormal];
    [self.menuCellImgBtn setImage:[UIImage imageNamed:@"m_search_s1"] forState:UIControlStateSelected];
}



@end
