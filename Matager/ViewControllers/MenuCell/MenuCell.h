//
//  MenuCell.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *menuCellTitle;
@property (weak, nonatomic) IBOutlet UIButton *menuCellImgBtn;


-(void)setCellWithTitle:(NSString *)title andImage:(NSString *)imgName;
-(void)setCellWithTitle:(NSString *)title ;
-(void)setCellWithDictionary:(NSDictionary *)dictionary;
-(void)setCellWithSearchTitle:(NSString *) title;

-(void)selectCellControls;

@end
