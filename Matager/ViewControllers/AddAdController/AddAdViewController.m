//
//  AddAdViewController.m
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "AddAdViewController.h"

@interface AddAdViewController ()

@end

@implementation AddAdViewController

@synthesize pagingScrollView_;
@synthesize pagings;
@synthesize table;
@synthesize lblManfacturer,lblConfirm,lblConfirmCode,lblContent,lblModel,lblYear;
@synthesize txtConfirmCode,txtContent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [lblManfacturer setBackgroundColor:UIColorFromRGBF(AddAdManfLabelBlackColor)];
        [lblModel setBackgroundColor:UIColorFromRGBF(AddAdModelLabelBlackColor)];
        [lblYear setBackgroundColor:UIColorFromRGBF(AddAdYearLabelBlackColor)];
        [lblContent setBackgroundColor:UIColorFromRGBF(AddAdContentLabelBlackColor)];
        [lblConfirmCode setBackgroundColor:UIColorFromRGBF(AddAdConfirmCodeLabelBlackColor)];
        [lblConfirm setBackgroundColor:UIColorFromRGBF(AddAdConfirmCodeLabelBlackColor)];
        
        [txtContent setBackgroundColor:UIColorFromRGBF(AddAdContentTextBlackColor)];
        [txtConfirmCode setBackgroundColor:UIColorFromRGBF(AddAdConfirmCodeTextBlackColor)];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setPaging];
    
    selectedImages = [[NSMutableArray alloc]init];
    //first object
    [selectedImages addObject:@""];
   // [pagingScrollView_ scrollRectToVisible:CGRectMake(0, 0, 960, 30) animated:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)chooseFile:(id)sender
{
    UIButton* btnSender = (UIButton*) sender;
    UIImagePickerController *pickerC = [[UIImagePickerController alloc] init];
    pickerC.delegate = self;
    
    
    imageIndex = btnSender.tag;
    [self presentViewController:pickerC animated:YES completion:nil];
    
}

-(IBAction)addNewPhoto:(id)sender
{
     [selectedImages addObject:@""];
    [self.table insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[selectedImages count]-1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(IBAction)removePhoto:(id)sender
{
    UIButton* btnSender = (UIButton*) sender;
    [selectedImages removeObjectAtIndex:btnSender.tag];
    [self.table deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:btnSender.tag  inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *gotImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    [selectedImages setObject:imageURL atIndexedSubscript:imageIndex];
    NSLog(@"%@", info);
}

- (void)imagePickerControllerDidCancel: (UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - paging control methods

- (void)createPagingControl {
    // programmatically add the page control
    pageControl_ = [[DDpageControl_ alloc] init];
	[pageControl_ setCenter: CGPointMake(self.view.center.x, self.view.bounds.size.height-20.0f)] ;
    //[self.view addSubview: pageControl_];
	[pageControl_ setNumberOfPages: 2];
	[pageControl_ addTarget: self action: @selector(changedValue:) forControlEvents: UIControlEventValueChanged] ;
	[pageControl_ setDefersCurrentPageDisplay: YES];
    [pageControl_ setHidesForSinglePage:YES];
    [pageControl_ setCurrentPage: 1];
	[pageControl_ setType: DDpageControl_TypeOnFullOffFull];
	//[pageControl_ setOnColor:UIColorFromRGB(ORANGE_COLOR)];
	//[pageControl_ setOffColor:UIColorFromRGB(GRAY_COLOR)];
	[pageControl_ setIndicatorDiameter: 10.0f];
	[pageControl_ setIndicatorSpace: 12];
    pageControlBeingUsed_ = NO;
}

-(void)setPaging{
    // add subViews to pageControl_lerScrolView
	
	pageControl_.currentPage=0;
	pagingScrollView_.pagingEnabled=YES;
	pagingScrollView_.contentOffset=CGPointMake(0, 0);
	pagingScrollView_.contentSize =
    CGSizeMake(pagingScrollView_.frame.size.width * 2,
               pagingScrollView_.frame.size.height);
	[pagingScrollView_ setDelegate:self];
}

-(IBAction)changedValue:(id) sender
{
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	pageControlBeingUsed_ = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	pageControlBeingUsed_ = NO;
    
    switch (pageControl_.currentPage) {
        case 0:
            [[pagings objectAtIndex:0] setSelected:YES];
            [[pagings objectAtIndex:1] setSelected:NO];

      //      [pageControlImageView_ setImage:[UIImage imageNamed:@"slider_3"]];
            break;
        case 1:
            [[pagings objectAtIndex:0] setSelected:NO];
            [[pagings objectAtIndex:1] setSelected:YES];
          //  [pageControlImageView_ setImage:[UIImage imageNamed:@"slider_2"]];
            break;
        default:
            break;
    }
}

-(IBAction)switchToPage:(UIButton *)sender
{
    for (UIButton * btn in self.pagings) {
        [btn setSelected:NO];
    }
    [sender setSelected:YES];
    switch (sender.tag) {
        case 1:
            [pageControl_ setCurrentPage:0];
            break;
        case 2:
            [pageControl_ setCurrentPage:1];
            break;
            
        default:
            break;
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
	if (!pageControlBeingUsed_) {
        // Switch the indicator when more than 50% of the previous/next page is visible
		CGFloat pageWidth = pagingScrollView_.frame.size.width;
		int page = floor((pagingScrollView_.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		pageControl_.currentPage = page;
        // if we are animating (triggered by clicking on the page control), we update the page control
		[pageControl_ updateCurrentPageDisplay];
        
	}
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
    // if we are animating (triggered by clicking on the page control), we update the page control
	[pageControl_ updateCurrentPageDisplay] ;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(section==0) return 1;
    else return [selectedImages count];
    //return [tempAds count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section]==0)
        return 260.0f;
    else
        return 99.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // static NSString *CellIdentifier = @"AdsCell";
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier  ];
    if([indexPath section]==0)
    {
        AddAdGeneralCell* gencell = [tableView dequeueReusableCellWithIdentifier:@"GeneralCell"];
        return gencell;
    }
    else
    {
        AddPhotoCell* photocell =   [tableView dequeueReusableCellWithIdentifier:@"AddPhotoCell"];
        photocell.tag = [indexPath row];//[selectedImages count];
        photocell.btnChooseFile.tag = [indexPath row];
        photocell.btnRemovePhoto.tag = [indexPath row];

        return photocell;
    }
//    [cell setCellDataWith:[tempAds objectAtIndex:indexPath.row]];
//    [cell.addToFavButton setTag:indexPath.row];
//    [cell.addToFavButton addTarget:self action:@selector(AddRemoveAdCellFromFav:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [cell.shareButton setTag:indexPath.row];
//    [cell.shareButton addTarget:self action:@selector(ShareAdCell:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}


@end
