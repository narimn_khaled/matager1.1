//
//  AddAdViewController.h
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"
#import "DDPageControl.h"
#import "AddAdGeneralCell.h"
#import "AddPhotoCell.h"

@interface AddAdViewController : MasterViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationBarDelegate>
{
    // pagingController data members
	DDpageControl_* pageControl_;
    
    bool pageControlBeingUsed_;
    
    /*
     *  Selected Images
     */
    NSMutableArray* selectedImages;
    
    int imageIndex;
}
@property(nonatomic,weak) IBOutlet UITableView* table;

@property(nonatomic,weak) IBOutlet UILabel* lblManfacturer;
@property(nonatomic,weak) IBOutlet UILabel* lblModel;
@property(nonatomic,weak) IBOutlet UILabel* lblYear;
@property(nonatomic,weak) IBOutlet UILabel* lblContent;
@property(nonatomic,weak) IBOutlet UILabel* lblConfirm;
@property(nonatomic,weak) IBOutlet UILabel* lblConfirmCode;
@property(nonatomic,weak) IBOutlet UITextView* txtContent;
@property(nonatomic,weak) IBOutlet UITextField* txtConfirmCode;

@property(nonatomic,weak) IBOutlet UIScrollView* pagingScrollView_;
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray* pagings;

@end
