//
//  HomeViewController.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "HomeViewController.h"
#import "ProductCell.h"
#import "SVProgressHUD.h"
#import "ProductsDataManager.h"

@interface HomeViewController () <ProductCellDelegate>
{
    NSMutableArray *dataSource;
}
@end

@implementation HomeViewController
@synthesize type;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
  //  [self.view setBackgroundColor:redColor];
    UINib *productNib = [UINib nibWithNibName:@"ProductCell" bundle:nil];
    [self.tableView registerNib:productNib
         forCellReuseIdentifier:@"ProductCell"];
    
    [self setPageTitle:@"test الرئيسية"];
    type =newProducts;
    
    [self loadDataByType];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark data loading functions 

-(void)loadDataByType {
    if (type==cityProducts) {
        NSString * cityId= [ProductsDataManager getProductsSavedCountryID];
        if (cityId == nil) {
            // show setting view
        }else{
            [ProductsDataManager getCityProductsWithCityID:cityId andProducts:^(NSMutableArray *productsMArray) {
                dataSource =productsMArray;
                if ([dataSource count]<=0) {
                    [self showNoDataViews];
                }else{
                    [self showDataViews];
                }

            }];
        }
    }else {
        [ProductsDataManager getProductsOfType:type WithResultedProducts:^(NSMutableArray *productsMArray) {
            dataSource =productsMArray;
            if ([dataSource count]<=0) {
                [self showNoDataViews];
            }else{
                [self showDataViews];
            }
        }];
    }
    
}


-(void)showNoDataViews{
    [self.noDataLabel setHidden:NO];
    [self.tableView setHidden:YES];
}

-(void)showDataViews{
    [self.noDataLabel setHidden:YES];
    [self.tableView setHidden:NO];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProductCell";
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    [cell setCellProduct:[dataSource objectAtIndex:indexPath.row] withImage:(type==noPicProducts?NO: YES) forCellWithIndex:indexPath.row];
        
    [cell setDelegate:self];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


#pragma -mark cell delegate
-(void)ProductCellGoToCountryProducts:(NSString *)countryID{
    [[[UIAlertView alloc]initWithTitle:@"alert" message:@"go to country" delegate:nil cancelButtonTitle:@"done" otherButtonTitles: nil]show];
}

-(void)ProductCellGoToUserProfile:(NSString *)userID{
    [[[UIAlertView alloc]initWithTitle:@"alert" message:@"go to profile" delegate:nil cancelButtonTitle:@"done" otherButtonTitles: nil]show];
}
- (IBAction)togglePageTypeButtonsTouched:(UIButton *)sender {
    type=[sender tag];
    
    [self.togglePageTypeButtons[0] setSelected:NO];
    [self.togglePageTypeButtons[1] setSelected:NO];
    [self.togglePageTypeButtons[2] setSelected:NO];
    
    [UIView animateWithDuration:.2 animations:^{
        [self.selectionImg setFrame:[sender frame]];
        [sender setSelected:YES];
    } completion:^(BOOL finished) {
        [self.tableView reloadData];
    }];
}
@end
