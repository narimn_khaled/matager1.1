//
//  HomeViewController.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"
#import "ProductModel.h"


@interface HomeViewController : MasterViewController <UITableViewDataSource , UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *togglePageTypeButtons;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImg;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
@property (nonatomic) enum homeProductType type;


- (IBAction)togglePageTypeButtonsTouched:(UIButton *)sender;
@end
