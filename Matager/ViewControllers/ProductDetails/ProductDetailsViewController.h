//
//  ProductDetailsViewController.h
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"

@interface ProductDetailsViewController : MasterViewController <UITableViewDataSource , UITableViewDelegate>

@property (nonatomic , retain)NSString * productID;
@property (nonatomic) BOOL isFromShop;


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
