//
//  ProductDetailsViewController.m
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "ProductDetailsViewController.h"
#import "ProductsDataManager.h"
#import "ProductDetailsCell.h"
#import "ProductDetailsSliderCell.h"
#import "CommentCell.h"

#import "ProductModel.h"
#import "CommentDataModel.h"

#import "SVProgressHUD.h"

@interface ProductDetailsViewController ()
{
    ProductModel * currentProduct;
    NSMutableArray * commentsMArray;
}
@end

@implementation ProductDetailsViewController
@synthesize productID;
@synthesize isFromShop;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [SVProgressHUD show];
    [ProductsDataManager getProductDetailsWithProductID:productID withProduct:^(ProductModel *product) {
        [ProductsDataManager getProductCommentsWithProductID:productID withProductComments:^(NSMutableArray *productComments) {
            commentsMArray=productComments;
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    } else if(section==1){
        //check page from which paret
        return  isFromShop? 2: 1;
    }else{
        return [commentsMArray count]<=0 ? 1 :[commentsMArray count];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return  [self getPostCellHeight];
            break;
        case 1:
            if (indexPath.row==0) {
                return  ([currentProduct.productImagesArray count]>0) ? 130.0 : 30.0;
            }else{
                return 130.0;
            }
            break;
        case 2:
            return  [self getCommentCellHeight:indexPath.row];
            break;
    }
    return 100.0;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==2) {
        UIView* commentsheaderView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 30)];
        [commentsheaderView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView * img=[[UIImageView alloc]initWithFrame:commentsheaderView.frame];
        [img setImage:[UIImage imageNamed:@"page_section"]];
        [commentsheaderView addSubview:img];
        
        UILabel * headerLabel=[[UILabel alloc]initWithFrame:commentsheaderView.frame];
        [headerLabel setText:@"التعليقات"];
        [headerLabel setTextAlignment:NSTextAlignmentRight];
        [headerLabel setTextColor:UIColorFromRGBF(ProductDetailsBabyBlueColor)];
        [headerLabel setFont:[UIFont systemFontOfSize:13]];
        [commentsheaderView addSubview:headerLabel];
        
        UIButton * addReplyButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [addReplyButton setFrame:CGRectMake(10, 2, 25, 25)];
        [addReplyButton setImage:[UIImage imageNamed:@"page_section_btn_comment_s0"] forState:UIControlStateNormal];
        [addReplyButton setImage:[UIImage imageNamed:@"page_section_btn_comment_s1"] forState:UIControlStateHighlighted];
        [addReplyButton addTarget:self action:@selector(commentSectionAddReplyButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
        [commentsheaderView addSubview:addReplyButton];
        
        return commentsheaderView;
    }else{
        return [[UIView alloc]initWithFrame:CGRectZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}






#pragma -mark tableView Row height helper functions
-(float)getPostCellHeight{
    if (currentProduct.productDescription.length>0) {
        
        NSString *theText = currentProduct.productDescription;
        NSLog(@" content : %@" , theText);
        CGSize theSize = [theText sizeWithFont:[UIFont systemFontOfSize:13 ] constrainedToSize:CGSizeMake(275,MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        
        float height=theSize.height + 170.0>210.0 ? theSize.height + 170.0 :210.0;
        NSLog(@"return height : %f " , height);
        return  height;
    }else{
        NSLog(@"return height : %f " , 210.0);
        return 210.0;
    }
    
}

-(float)getCommentCellHeight:(int)index{
    NSLog(@"index : %i " , index);
    
    CommentDataModel *comment = commentsMArray[index];
    NSString *theText = comment.commentContent;
    NSLog(@"index : %i  and content : %@" , index , theText);
    CGSize theSize = [theText sizeWithFont:[UIFont systemFontOfSize:14 ] constrainedToSize:CGSizeMake(275,MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    float commentHeight=theSize.height+50.0>100 ? theSize.height+50.0 : 100.0;
    NSLog(@"comment WzNo chils height : %f " , commentHeight);
    if (comment.commentHaveChild) {
        NSString * childContent=comment.childCommentContent;
        CGSize childSize=[childContent sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(170, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        float childHeight=childSize.height+50>100.0 ? childSize.height+50.0 : 100.0;
        commentHeight =commentHeight +childHeight;
    }
    return commentHeight;
    
    
}


#pragma -mark CellSection Button Action
-(IBAction)commentSectionAddReplyButtonTouched:(id)sender{
    
    
}





@end
