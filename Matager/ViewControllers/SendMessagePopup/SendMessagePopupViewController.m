//
//  SendMessagePopupViewController.m
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "SendMessagePopupViewController.h"

@interface SendMessagePopupViewController ()

@end

@implementation SendMessagePopupViewController
@synthesize messageToReplyTo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)SendMessageButtonTouched:(UIButton *)sender {
}

- (IBAction)backgroundTouched:(UIControl *)sender {
    [self hideKeyBoard:nil];
    
    [self.messageSubjectText setText:@""];
    [self.messageTitleText setText:@""];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)hideKeyBoard:(UIControl *)sender {
    [self.messageSubjectText resignFirstResponder];
    [self.messageTitleText resignFirstResponder];
}



- (IBAction)cancelButtonTouched:(UIButton *)sender {
    [self backgroundTouched:nil];
}
@end
