//
//  SendMessagePopupViewController.h
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageDataModel.h"

@interface SendMessagePopupViewController : UIViewController

@property (nonatomic , assign) MessageDataModel * messageToReplyTo;

@property (weak, nonatomic) IBOutlet UITextField *messageTitleText;

@property (weak, nonatomic) IBOutlet UITextView *messageSubjectText;






- (IBAction)SendMessageButtonTouched:(UIButton *)sender;

- (IBAction)backgroundTouched:(UIControl *)sender;


- (IBAction)hideKeyBoard:(UIControl *)sender;

- (IBAction)cancelButtonTouched:(UIButton *)sender;

@end
