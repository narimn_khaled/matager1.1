//
//  MasterViewController.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"
#import "IIViewDeckController.h"

#import "colors.h"

@interface MasterViewController (){
    UIButton * backButton;
    UILabel * headerLabel;
    UIButton * menuButton;
}
@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]]];
	// Do any additional setup after loading the view.
    [self.headerView setFrame:CGRectMake(0, 0, 320, 44)];
    
    UIImageView * bgImage=[[UIImageView alloc]initWithFrame:self.headerView.frame];
    [bgImage setImage:[UIImage imageNamed:@"header_bg"]];
    [self.headerView addSubview:bgImage];
    
    headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(50, 10, 220, 24)];
    [headerLabel setTextColor:UIColorFromRGBF(headerTitleColor)];
    [headerLabel setBackgroundColor:[UIColor clearColor]];
    [headerLabel setTextAlignment:NSTextAlignmentCenter];
    //[headerLabel setFont:[UIFont fontWithName:@"BArshia" size:19]];
    [headerLabel setFont:[UIFont systemFontOfSize:21]];
    [self.headerView addSubview:headerLabel];
    
//    NSArray * fonts =[UIFont familyNames];
//    NSLog(@"fonts : %@ . %@" , fonts);
    
    menuButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setFrame:CGRectMake(280, 7, 30, 30)];
    [menuButton setImage:[UIImage imageNamed:@"header_btn_menu_s0"] forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"header_btn_menu_s1"] forState:UIControlStateHighlighted];
    [menuButton addTarget:self action:@selector(headerMenuButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:menuButton];
    
    backButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(10, 7, 30, 30)];
    [backButton setImage:[UIImage imageNamed:@"header_btn_back_s0"] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"header_btn_back_s1"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(headerBackButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:backButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(UIColor *)getUIColorFromRGBX(rgbValue) {
    return  [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
}
*/


-(void)setPageTitle:(NSString *)title{
    [headerLabel setText:title];
}
-(void)showSubTitleWithTitle:(NSString *)pageTitle{
    [self.headerView setFrame:CGRectMake(0, 0, 320, 74)];
    UIImageView * subTitleBg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"header_subtitle_bg"]];
    [subTitleBg setFrame:CGRectMake(0, 44, 320, 30)];
    [self.headerView addSubview:subTitleBg];
    
    UILabel * subtitleLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 44, 300, 30)];
    [subtitleLbl setText:pageTitle];
    [subtitleLbl setTextColor:UIColorFromRGBF(subHeaderTitleColor)];
    [subtitleLbl setBackgroundColor:[UIColor clearColor]];
    [subtitleLbl setFont:[UIFont systemFontOfSize:14]];
    [subtitleLbl setTextAlignment:NSTextAlignmentRight];
    [self.headerView addSubview:subtitleLbl];
    
}

-(IBAction)headerMenuButtonTouched:(id)sender{
    [self.viewDeckController toggleRightViewAnimated:YES];
}

-(IBAction)headerBackButtonTouched:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setBackButtonHidden:(BOOL)hidden{
    [backButton setHidden:hidden];
}

@end






