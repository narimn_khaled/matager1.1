//
//  MasterViewController.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "colors.h"

@interface MasterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *headerView;

-(void)setPageTitle:(NSString *)title;
-(void)showSubTitleWithTitle:(NSString *)pageTitle;

-(void)setBackButtonHidden:(BOOL)hidden;
-(void)setMenuButtonHidden:(BOOL)hidden;

-(IBAction)headerBackButtonTouched:(id)sender;
@end
