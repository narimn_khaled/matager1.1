//
//  MessagesViewController.m
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessageDataModel.h"
#import "MessagesCell.h"
#import "UserDataManager.h"
#import "MessageDetailsViewController.h"

#import "SVProgressHUD.h"
#import "FrameAccessor.h"

#import "MenuViewController.h"
#import "IIViewDeckController.h"

@interface MessagesViewController () <ZKRevealingTableViewCellDelegate ,MenuViewControllerDelegate>
{
    NSMutableArray * messagesMArray;
}
@end

@implementation MessagesViewController
@synthesize isUserInbox;
@synthesize revealedIndex=_revealedIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [((MenuViewController*)self.viewDeckController.rightController) setDelegate:self];
    [self LoadMessagesData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)LoadMessagesData{
    [SVProgressHUD show];
    messagesMArray=[[NSMutableArray alloc]init];
    if (isUserInbox) {
        [self setPageTitle:@"الرسائل المستلمة"];
        [self showSubTitleWithTitle:@"رسائلى المستلمة"];
    }else{
        [self setPageTitle:@"الرسائل المرسلة"];
        [self showSubTitleWithTitle:@"رسائلى المرسلة"];
    }
    [[UserDataManager SharedInstance]getUserMessagesForInbox:isUserInbox WithMessages:^(NSMutableArray *messages) {
        if ([messages count]>0) {
            messagesMArray =[[NSMutableArray alloc]initWithArray:messages];
            [self.tabelView reloadData];
            [self.noDataLabel setHidden:YES];
        }else{
            [self.tabelView setHidden:YES];
            [self.noDataLabel setHidden:NO];
        }
        [SVProgressHUD dismiss];
    }];
}
#pragma -mark menuController delegte
-(void)MenuViewControllerSelectedMessagesOfInbox:(BOOL)isInbox{
    self.isUserInbox = isInbox;
    [self LoadMessagesData];
}

#pragma -mark Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    MessageDetailsViewController * details=[segue destinationViewController];
    [details setMessage:[messagesMArray objectAtIndex:[sender tag]]];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [messagesMArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessagesCell";
    MessagesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [cell setCellMessageDataWithMessage:[messagesMArray objectAtIndex:indexPath.row] forIndex:indexPath.row ];
    [cell.messageSenderButton addTarget:self action:@selector(goToSenderProfile:) forControlEvents:UIControlEventTouchUpInside];
    if (!cell.backView.subviews.count) {
        [self setupBackView:cell.backView forMessage:cell];
    }
    return cell;
}

-(BOOL)cellShouldReveal:(ZKRevealingTableViewCell *)cell
{
    return YES;
}

-(void)cellWillRevealBackview:(ZKRevealingTableViewCell *)cell
{
    cell.backView.alpha = 0.0;
}

-(void)cellDidRevealBackview:(ZKRevealingTableViewCell *)cell
{
    [UIView animateWithDuration:0.25 animations:^{
        cell.backView.alpha = 1.0;
    }];
    
    self.revealedIndex = [self.tabelView indexPathForCell:cell];
}

-(void)cellWillHideBackview:(ZKRevealingTableViewCell *)cell
{
    [UIView animateWithDuration:0.5 animations:^{
        cell.backView.alpha = 0.0;
    }];
}

-(void)setupBackView:(UIView *)back  forMessage:(MessagesCell *)Message
{
    UIButton *delete = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [delete setImage:[UIImage imageNamed:@"popup_delete_btn_delete_s0"] forState:UIControlStateNormal];
    [delete setImage:[UIImage imageNamed:@"popup_delete_btn_delete_s1"] forState:UIControlStateHighlighted];
    [delete setImage:[UIImage imageNamed:@"popup_delete_btn_delete_s1"] forState:UIControlStateSelected];
    
    [delete addTarget:self action:@selector(deleteMessageButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    delete.size = CGSizeMake(115, 35);
    delete.center = CGPointMake(back.width/2, back.height/2);
    

    [back addSubview:delete];
}

-(IBAction)deleteMessageButtonTouched:(UIButton *)sender
{
    MessagesCell *cell = (MessagesCell *)sender.superview.superview;
    NSIndexPath *index = [self.tabelView indexPathForCell:cell];
    
    MessageDataModel * messageToDelete=[messagesMArray objectAtIndex:index.row];
    [[UserDataManager SharedInstance]deleteUserMessage:messageToDelete WithSuccess:^(BOOL doneDeleting) {
        [messagesMArray removeObjectAtIndex:index.row];
        [self.tabelView deleteRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationLeft];
    }];
}



#pragma mark - Accessors

- (ZKRevealingTableViewCell *)currentlyRevealedCell
{
    return (ZKRevealingTableViewCell *)[self.tabelView cellForRowAtIndexPath:self.revealedIndex];
}

-(void)setRevealedIndex:(NSIndexPath *)revealedIndex
{
    if ([_revealedIndex isEqual:revealedIndex])
        return;
    
    [[self currentlyRevealedCell] setRevealing:NO];
    [self currentlyRevealedCell].backView.alpha = 0.0;
    
    _revealedIndex = revealedIndex;
}




#pragma -cell action
-(IBAction)goToSenderProfile:(UIButton*)sender{
    NSString * userID=[NSString  stringWithFormat:@"%i", sender.tag];
    // go to this user id profile 
}

@end
