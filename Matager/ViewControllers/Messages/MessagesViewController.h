//
//  MessagesViewController.h
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"

@interface MessagesViewController : MasterViewController <UITableViewDataSource , UITableViewDelegate>
{

}
@property (weak, nonatomic) IBOutlet UITableView *tabelView;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@property (strong, nonatomic) NSIndexPath *revealedIndex;

@property (nonatomic) BOOL isUserInbox;
@end
