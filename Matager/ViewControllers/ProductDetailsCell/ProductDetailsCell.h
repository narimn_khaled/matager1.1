//
//  ProductDetailsCell.h
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductDetailsCellDelegate <NSObject>

-(void)ProductDetailsCellGoToOwnerProfile;
-(void)ProductDetailsCellGoToProductCity;

-(void)ProductDetailsCellShareProduct;
-(void)ProductDetailsCellReportProduct;
-(void)ProductDetailsCellSendMessageToOwner;
-(void)ProductDetailsCellGetProductInfo;
-(void)ProductDetailsCellAddOrRemoveFavorite;




@end

@interface ProductDetailsCell : UITableViewCell
{
    __weak IBOutlet UILabel *sttc_fromLabel;
    __weak IBOutlet UILabel *sttc_productNumberLabel;
    __weak IBOutlet UILabel *sttc_viewCountLabel;
    
    __weak IBOutlet UILabel *fromTimeLabel;
    __weak IBOutlet UILabel *stateLabel;
    __weak IBOutlet UILabel *productNumberLabel;
    __weak IBOutlet UILabel *viewCountLabel;
    
    __weak IBOutlet UILabel *descriptionText;

    __weak IBOutlet UIImageView *productImage;
    
    __weak IBOutlet UIButton *productOwnerName;
    __weak IBOutlet UIButton *productCity;
    
    
    __weak IBOutlet UIView *visitorButtonView;
    
}


- (IBAction)addRemoveProductFromFavButtonTouched:(UIButton *)sender;
- (IBAction)ShareProductButtonTouched:(UIButton *)sender;
- (IBAction)SendMessageToProductOwnerButtonTouched:(UIButton *)sender;
- (IBAction)getProductInfoButtonTouched:(UIButton *)sender;
- (IBAction)reportProductButtonTouched:(UIButton *)sender;


- (IBAction)deleteOrRestoreProductButtonTouched:(UIButton *)sender;
- (IBAction)editProductButtonTouched:(UIButton *)sender;


@end
