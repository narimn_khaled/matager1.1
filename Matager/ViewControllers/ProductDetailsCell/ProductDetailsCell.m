//
//  ProductDetailsCell.m
//  Matager
//
//  Created by Artgine Corp. on 11/19/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "ProductDetailsCell.h"

@implementation ProductDetailsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)addRemoveProductFromFavButtonTouched:(UIButton *)sender {
}

- (IBAction)ShareProductButtonTouched:(UIButton *)sender {
}

- (IBAction)SendMessageToProductOwnerButtonTouched:(UIButton *)sender {
}

- (IBAction)getProductInfoButtonTouched:(UIButton *)sender {
}

- (IBAction)reportProductButtonTouched:(UIButton *)sender {
}

- (IBAction)deleteOrRestoreProductButtonTouched:(UIButton *)sender {
}

- (IBAction)editProductButtonTouched:(UIButton *)sender {
}
@end
