//
//  ProductCell.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "ProductCell.h"
#import "colors.h"

@interface ProductCell()
{
    ProductModel * cellProduct;
}
@end

@implementation ProductCell

@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if (selected) {
        [bgImage setImage:[UIImage imageNamed:@"home_list_item_hover"]];
    }else{
        [self setCellBackgroundWithIndex:[self tag]];
    }
}

-(void)setCellProduct:(ProductModel *)Product withImage:(BOOL)haveImage forCellWithIndex:(int)cellIndex{

    [self setCellControlsWithImage:haveImage];
    [self setCellControlsColors];
    [self setCellBackgroundWithIndex:cellIndex];
    [self setTag:cellIndex];
    
    cellProduct =Product;
    [self setCellDataWithImage:haveImage];
}

- (IBAction)goToOwnerProfileButtonsTouched:(UIButton *)sender {
    if ([delegate respondsToSelector:@selector(ProductCellGoToUserProfile:)]) {
        [delegate ProductCellGoToUserProfile:cellProduct.productOwnerID];
    }
}

- (IBAction)goToCountryProductsButtonsTouched:(UIButton *)sender {
    if ([delegate respondsToSelector:@selector(ProductCellGoToCountryProducts:)]) {
        [delegate ProductCellGoToCountryProducts:cellProduct.productCounrtyID];
    }
}

-(void)setCellDataWithImage:(BOOL)haveImage{
    if (haveImage) {
        //load image
    }
    // check if the topic special topic to view the sign
     // remove the have image check with the is special check 
    if (haveImage) {
        [productSpecialMark setHidden:NO];
        [productArrow setHidden:YES];
    }else{
        [productSpecialMark setHidden:YES];
        [productArrow setHidden:NO];
    }
    
    // show the product data in the controls 
    
}

-(void)setCellControlsColors{
    [productTitle setTextColor:UIColorFromRGBF(ProductCellTitleColor)];
    [productType setTitleColor:UIColorFromRGBF(ProductCell3ardColor) forState:UIControlStateNormal];
    [productType setTitleColor:UIColorFromRGBF(ProductCellTalabColor) forState:UIControlStateSelected];
    
    [productOwnerNameButton setTitleColor:UIColorFromRGBF(ProductCellTextButtonsNormalColor) forState:UIControlStateNormal];
    [productOwnerNameButton setTitleColor:UIColorFromRGBF(ProductCellTextButtonsSelectedColor) forState:UIControlStateSelected];
    
    [productCountryNameButton setTitleColor:UIColorFromRGBF(ProductCellTextButtonsNormalColor) forState:UIControlStateNormal];
    [productCountryNameButton setTitleColor:UIColorFromRGBF(ProductCellTextButtonsSelectedColor) forState:UIControlStateSelected];
    
    [productDurationTextButton setTitleColor:UIColorFromRGBF(ProductCellTextButtonsNormalColor) forState:UIControlStateNormal];
    [productDurationTextButton setTitleColor:UIColorFromRGBF(ProductCellTextButtonsSelectedColor) forState:UIControlStateSelected];
}

-(void)setCellControlsWithImage:(BOOL)haveImage{
    if (!haveImage) {
        [infoView setFrame:CGRectMake(infoView.frame.origin.x, infoView.frame.origin.y, 295, infoView.frame.size.height)];
        [productImage setHidden:YES];
        [productImageFrame setHidden:YES];
    }else{
        [infoView setFrame:CGRectMake(infoView.frame.origin.x, infoView.frame.origin.y, 250, infoView.frame.size.height)];
        [productImage setHidden:NO];
        [productImageFrame setHidden:NO];
    }
}

-(void)setCellBackgroundWithIndex:(int)index{
    if (index%2) {
        [bgImage setImage:[UIImage imageNamed:@"home_list_item_white"]];
    }else{
        [bgImage setImage:[UIImage imageNamed:@"home_list_item__gray"]];
    }
}

@end
