//
//  ProductCell.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

@protocol ProductCellDelegate <NSObject>

-(void)ProductCellGoToUserProfile:(NSString *)userID;
-(void)ProductCellGoToCountryProducts:(NSString *)countryID;
@end

@interface ProductCell : UITableViewCell
{
    __weak IBOutlet UIImageView *bgImage;
    
    __weak IBOutlet UIView *infoView;
    __weak IBOutlet UIImageView *productImage;
    __weak IBOutlet UIImageView *productImageFrame;
    
    __weak IBOutlet UILabel *productTitle;
    __weak IBOutlet UIButton *productType;
    
    __weak IBOutlet UIButton *productOwnerNameButton;
    __weak IBOutlet UIButton *productOwnerIconButton;
    
    __weak IBOutlet UIButton *productCountryNameButton;
    __weak IBOutlet UIButton *productCountryIconButton;
    
    __weak IBOutlet UIButton *productDurationTextButton;
    __weak IBOutlet UIButton *productDurationIconButton;
    
    __weak IBOutlet UIImageView *productSpecialMark;
    __weak IBOutlet UIImageView *productArrow;
    
    
}



@property (nonatomic , assign) id<ProductCellDelegate> delegate;



-(void)setCellProduct:(ProductModel *)cellProduct withImage:(BOOL)haveImage forCellWithIndex:(int)cellIndex;

- (IBAction)goToOwnerProfileButtonsTouched:(UIButton *)sender;

- (IBAction)goToCountryProductsButtonsTouched:(UIButton *)sender;


@end
