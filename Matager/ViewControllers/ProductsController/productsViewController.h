//
//  productsViewController.h
//  Matager
//
//  Created by Artgine Corp. on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"

@interface productsViewController : MasterViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
