//
//  MenuViewController.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MenuViewController.h"
#import "UserDataManager.h"
#import "colors.h"
#import "MenuCell.h"

#import "IIViewDeckController.h"

#import "MessagesViewController.h"
#import "LoginViewController.h"

@interface MenuViewController ()
@property (nonatomic , retain) NSMutableArray * menuStaticItems;
@property (nonatomic ,retain) NSMutableArray * menuCategoryItems;
@property (nonatomic , retain) NSMutableArray * menuSearchItems;
@property (nonatomic , retain) NSMutableArray * menuExtraItems;
@end

@implementation MenuViewController
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UserDataManager SharedInstance]setDelegate:self];
    
    [self setMenuExtraItem];
    [self setMenuCategoryItem];
    [self setMenuSearchItem];
    [self setMenuStaticItems];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark  menu items functions
-(void)setMenuStaticItems{
    self.menuStaticItems =[NSMutableArray new];
    if([[UserDataManager SharedInstance]isUserLogged]){
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"الصفحة الرئيسية",@"title",@"m_home_s0@2x",@"img",@"m_home_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"الاعلانات المصورة",@"title",@"m_pics_s0@2x",@"img",@"m_pics_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"تعديل البيانات",@"title",@"m_edit_details_s0@2x",@"img",@"m_edit_details_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"اعلاناتى",@"title",@"m_my_adv_s0@2x",@"img",@"m_my_adv_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"متاجرى",@"title",@"m_my_stores_s0@2x",@"img",@"m_my_stores_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"اضافة اعلان",@"title",@"m_add_adv_s0@2x",@"img",@"m_add_adv_s0@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"الاعلانات المفضلة",@"title",@"m_fav_s0@2x",@"img",@"m_fav_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"الرسائل المستلمة",@"title",@"m_sent_rcvd_s0@2x",@"img",@"m_sent_rcvd_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"الرسائل المرسلة",@"title",@"m_sent_msg_s0@2x",@"img",@"m_sent_msg_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"تسجيل الخروج",@"title",@"m_log_out_s0@2x",@"img",@"m_log_out_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"اتصل بنا",@"title",@"m_call_us_s0@2x",@"img",@"m_call_us_s1@2x",@"img_s", nil]];
    }else{
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"الصفحة الرئيسية",@"title",@"m_home_s0@2x",@"img",@"m_home_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"الاعلانات المصورة",@"title",@"m_pics_s0@2x",@"img",@"m_pics_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"تسجيل الدخول",@"title",@"m_log_out_s0@2x",@"img",@"m_log_out_s1@2x",@"img_s", nil]];
        [self.menuStaticItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"اتصل بنا",@"title",@"m_call_us_s0@2x",@"img",@"m_call_us_s1@2x",@"img_s", nil]];
    }
    
    
    [self.tableView reloadData];
}

-(void)setMenuExtraItem{
    self.menuExtraItems =[[NSMutableArray alloc]init];
    
    [self.menuExtraItems addObject:@"page"];
    [self.menuExtraItems addObject:@"page"];
    [self.menuExtraItems addObject:@"page"];
}

-(void) setMenuCategoryItem{
    self.menuCategoryItems =[[NSMutableArray alloc]init];
    // get categories from link
    // add to the categories array
    [self.menuCategoryItems addObject:@"حراج السيارات"];
    [self.menuCategoryItems addObject:@"حراج العقارات"];
    [self.menuCategoryItems addObject:@"الحراج العام"];
    
}

-(void)setMenuSearchItem{
    self.menuSearchItems =[[NSMutableArray alloc]init];
    [self.menuSearchItems addObject:@"بحث عن السيارات"];
    [self.menuSearchItems addObject:@"بحث عن العقارات"];
    [self.menuSearchItems addObject:@"بحث في الحراج العام"];
    [self.menuSearchItems addObject:@"بحث عن عضو"];
    [self.menuSearchItems addObject:@"بحث عن متجر"];
}








#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [self.menuStaticItems count];
            break;
        case 1:
            return [self.menuExtraItems count];
            break;
        case 2:
            return [self.menuCategoryItems count];;
            break;
        case 3:
            return [self.menuSearchItems count];
            break;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  35;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 28;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView * headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 28)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView * headerImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 27)];
    [headerImage setImage:[UIImage imageNamed:@"side_menu_header"]];
    [headerView addSubview:headerImage];
    
    UILabel * headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 5, 290, 21)];
    [headerLabel setFont:[UIFont boldSystemFontOfSize:12]];//[UIFont fontWithName:@"DroidSansArabic" size:15]];//
    [headerLabel setTextColor:[UIColor UIColorFromRGB(sideMenuItemColor)]];
    [headerLabel setTextAlignment:NSTextAlignmentRight];
    [headerLabel setBackgroundColor:[UIColor clearColor]];
    NSString * str=@"";
    if (section==0) {
        str =@"القائمة الرئيسية";
    } else if (section==1){
        str =@"صفحات اضافية";
    }else if (section==2){
        str =@"قائمة الاقسام";
    }else{
        str =@"قائمة البحث";
    }
    [headerLabel setText:str];
    [headerView addSubview:headerLabel];
    [headerView bringSubviewToFront:headerLabel];
    
    
    UIImageView * bulletImg=[[UIImageView alloc]initWithFrame:CGRectMake(300, 10, 9, 9)];
    [bulletImg setImage:[UIImage imageNamed:@"side_menu_header_icon"]];
    [headerView addSubview:bulletImg];
    
    UIImageView * footerImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 27, 320, 1)];
    [footerImg setImage:[UIImage imageNamed:@"side_menu_header_separator_1px"]];
    [headerView addSubview:footerImg];
    
    return headerView;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier =@"MenuCell";
    
    MenuCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    if (indexPath.section==0) {
        [cell setCellWithDictionary:[self.menuStaticItems objectAtIndex:indexPath.row]];
    }else if(indexPath.section==1) {
        [cell setCellWithTitle:[self.menuExtraItems objectAtIndex:indexPath.row]];
    }else if(indexPath.section==2) {
        [cell setCellWithTitle:[self.menuCategoryItems objectAtIndex:indexPath.row]];
    }else {
        [cell setCellWithSearchTitle:[self.menuSearchItems objectAtIndex:indexPath.row]];
    }
    return cell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            // menu statis item section . selection action changes according to user login state
            if (indexPath.row==0) {
                // pop to home view
            }else if (indexPath.row==1) {
                // navigate to products with pics
                
            }else{
                //push to the view according to logging state
                if ([[UserDataManager SharedInstance]isUserLogged]) {
                    [self loggedUserSelectIndex:indexPath.row];
                }else{
                    [self notloggedUserSelectIndex:indexPath.row];
                }
            }
            break;
            
        case 1:
            
            break;
    }
    [self.viewDeckController toggleRightViewAnimated:YES];
}

-(void)loggedUserSelectIndex:(int)index{
    switch (index) {
        case 2:
            //edit info
            break;
        case 3:
            //my products
            break;
        case 4:
            //my shops
            break;
        case 5:
            //add product 
            break;
        case 6:
            //favorite ads  
            break;
        case 7:
            //inbox
            [self NavigateToMessagesViewWithTypeInbox:YES];
            break;
        case 8:
            //outbox
            [self NavigateToMessagesViewWithTypeInbox:NO];
            break;
        case 9:
            //sign out
            break;
        case 10:
            //call us 
            break;
    }
}
-(void)notloggedUserSelectIndex:(int)index{
    switch (index) {
        case 2:
            // sign in
            if([((UINavigationController*)self.viewDeckController.centerController).visibleViewController isKindOfClass:[LoginViewController class]]){
                
            }else{
                UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                
                LoginViewController * loginController =[storybord instantiateViewControllerWithIdentifier:@"LoginViewController"];
                
                [((UINavigationController*)self.viewDeckController.centerController) pushViewController:loginController animated:YES];
            }
            break;
        case 3:
            //contact us
            break;
    }
}




-(void)NavigateToController:(UIViewController *)controller Class:(Class) controllerClass andIdentifier:(NSString *)identifier {
    
    
    if([((UINavigationController*)self.viewDeckController.centerController).visibleViewController isKindOfClass:controllerClass]){
        
    }else{
        UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        
        controller =[storybord instantiateViewControllerWithIdentifier:identifier];
        
        [((UINavigationController*)self.viewDeckController.centerController) pushViewController:controller animated:YES];
    }
    
}



-(void)NavigateToMessagesViewWithTypeInbox:(BOOL)isInbox{
    
    
    if([((UINavigationController*)self.viewDeckController.centerController).visibleViewController isKindOfClass:[MessagesViewController class]]){
        if( [self.delegate respondsToSelector:@selector(MenuViewControllerSelectedMessagesOfInbox:)]){
            [self.delegate MenuViewControllerSelectedMessagesOfInbox:isInbox];
        }
    }else{
        UIStoryboard *storybord = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MessagesViewController* messages =[storybord instantiateViewControllerWithIdentifier:@"MessagesViewController"];
        [messages setIsUserInbox:isInbox];
        [((UINavigationController*)self.viewDeckController.centerController) pushViewController:messages animated:YES];
    }
    
}
-(void)NavigateToCatgoryViews{
    /*
     if([((UINavigationController*)self.viewDeckController.centerController).visibleViewController isKindOfClass:controllerClass]){
     if( [self.delegate respondsToSelector:@selector(menu)]){
     [self.delegate MenuViewControllerCategorySelected:[[DynamicObjects objectAtIndex:indexPath.row] objectForKey:@"name"] withID: [[DynamicObjects objectAtIndex:indexPath.row] objectForKey:@"ID"]];
     }
     }else{
     categoryAdsView =[storybord instantiateViewControllerWithIdentifier:@"CategoryAdsViewController"];
     [categoryAdsView setSectionID:[[DynamicObjects objectAtIndex:indexPath.row] objectForKey:@"ID"]];
     [((UINavigationController*)self.viewDeckController.centerController) pushViewController:categoryAdsView animated:YES];
     }*/
    
}
-(void)NavigateToSearchCatgoryViews{
    /*
     if([((UINavigationController*)self.viewDeckController.centerController).visibleViewController isKindOfClass:controllerClass]){
     if( [self.delegate respondsToSelector:@selector(menu)]){
     [self.delegate MenuViewControllerCategorySelected:[[DynamicObjects objectAtIndex:indexPath.row] objectForKey:@"name"] withID: [[DynamicObjects objectAtIndex:indexPath.row] objectForKey:@"ID"]];
     }
     }else{
     categoryAdsView =[storybord instantiateViewControllerWithIdentifier:@"CategoryAdsViewController"];
     [categoryAdsView setSectionID:[[DynamicObjects objectAtIndex:indexPath.row] objectForKey:@"ID"]];
     [((UINavigationController*)self.viewDeckController.centerController) pushViewController:categoryAdsView animated:YES];
     }*/
    
}

#pragma -mark userManager delegate
-(void)UserDataManagerUserLoginStatusChanged{
    [self setMenuStaticItems];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
