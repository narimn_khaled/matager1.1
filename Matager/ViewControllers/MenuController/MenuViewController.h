//
//  MenuViewController.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataManager.h"

@protocol MenuViewControllerDelegate <NSObject>

-(void)MenuViewControllerSelectedMessagesOfInbox:(BOOL)isInbox;
-(void)MenuViewControllerSelectedSearchCategory:(NSString *)searchCategory;
-(void)MenuViewControllerSelectedCategory:(NSString *)Category;
@end

@interface MenuViewController : UITableViewController <UserDataManagerDelegate>
@property (nonatomic , assign) id<MenuViewControllerDelegate> delegate;
@end
