//
//  ViewController.h
//  Matager
//
//  Created by Ali Amin on 11/11/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface ViewController : IIViewDeckController

@end
