//
//  LoginViewController.h
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "MasterViewController.h"

@interface LoginViewController : MasterViewController

- (IBAction)logUserInButtonTouched:(UIButton *)sender;
@end
