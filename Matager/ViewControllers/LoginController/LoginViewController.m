//
//  LoginViewController.m
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/18/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "LoginViewController.h"
#import "UserDataManager.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logUserInButtonTouched:(UIButton *)sender {
    [[UserDataManager SharedInstance]logUserInWithUserName:@"" andPassword:@""];
}



@end
