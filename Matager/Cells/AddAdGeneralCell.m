//
//  AddAdGeneralCell.m
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/25/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "AddAdGeneralCell.h"

@implementation AddAdGeneralCell

@synthesize AdLocation;
@synthesize AdTypes;
@synthesize lblAdTitle,lblAdType,lblCommunicationType,lblCountry,lblLocation;
@synthesize txtAdtitle,txtCommunicationType,txtCountry;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [lblAdTitle setBackgroundColor:UIColorFromRGBF(AddAdAdTitlelabelBlackColor)];
        [lblAdType setBackgroundColor:UIColorFromRGBF(AddAdAdTypeLabelLBlackColor)];
        [lblCommunicationType setBackgroundColor:UIColorFromRGBF(AddAdCommunicationLabelBlackColor)];
        [lblCountry setBackgroundColor:UIColorFromRGBF(AddAdCountryLabelBlackColor)];
        [lblLocation setBackgroundColor:UIColorFromRGBF(AddAdLocationLabelBlackColor)];
        
        [txtAdtitle setBackgroundColor:UIColorFromRGBF(AddAdAdTitleTextBlackColor)];
        [txtCommunicationType setBackgroundColor:UIColorFromRGBF(AddAdCommunicationTextBlackColor)];
        [txtCountry setBackgroundColor:UIColorFromRGBF(AddAdConutryTextBlackColor)];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)adSelectionOptionPressed:(UIButton *)sender
{
    for (UIButton * btn in self.AdTypes) {
        [btn setSelected:NO];
    }
    [sender setSelected:YES];
//    switch (sender.tag) {
//        case 1:
//            [pageControl_ setCurrentPage:0];
//            break;
//        case 2:
//            [pageControl_ setCurrentPage:1];
//            break;
//            
//        default:
//            break;
//    }
}
-(IBAction)adLocationOptionPressed:(UIButton *)sender
{
    for (UIButton * btn in self.AdLocation) {
        [btn setSelected:NO];
    }
    [sender setSelected:YES];
//    switch (sender.tag) {
//        case 1:
//            [pageControl_ setCurrentPage:0];
//            break;
//        case 2:
//            [pageControl_ setCurrentPage:1];
//            break;
//            
//        default:
//            break;
//    }
}
@end
