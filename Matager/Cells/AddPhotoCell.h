//
//  AddPhotoCell.h
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/25/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "colors.h"

@interface AddPhotoCell : UITableViewCell

@property(nonatomic,retain) IBOutlet UIButton* btnRemovePhoto;
@property(nonatomic,retain) IBOutlet UIButton* btnChooseFile;
@property(nonatomic,retain) IBOutlet UILabel* lblImageUrl;
@property(nonatomic,retain) IBOutlet UILabel* lblImageTitle;

@end
