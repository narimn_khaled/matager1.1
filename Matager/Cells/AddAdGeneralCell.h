//
//  AddAdGeneralCell.h
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/25/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "colors.h"

@interface AddAdGeneralCell : UITableViewCell

@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray* AdTypes;
@property(nonatomic,strong) IBOutletCollection(UIButton) NSArray* AdLocation;

@property(nonatomic,strong) IBOutlet UILabel* lblAdType;
@property(nonatomic,strong) IBOutlet UILabel* lblAdTitle;
@property(nonatomic,strong) IBOutlet UILabel* lblCommunicationType;
@property(nonatomic,strong) IBOutlet UILabel* lblCountry;
@property(nonatomic,strong) IBOutlet UILabel* lblLocation;

@property(nonatomic,strong) IBOutlet UITextField* txtAdtitle;
@property(nonatomic,strong) IBOutlet UITextField* txtCommunicationType;
@property(nonatomic,strong) IBOutlet UITextField* txtCountry;


@end
