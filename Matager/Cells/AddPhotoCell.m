//
//  AddPhotoCell.m
//  Matager
//
//  Created by Abdelrahman Mohamed on 11/25/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "AddPhotoCell.h"

@implementation AddPhotoCell

@synthesize btnRemovePhoto;
@synthesize btnChooseFile;
@synthesize lblImageTitle,lblImageUrl;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [lblImageTitle setBackgroundColor:UIColorFromRGBF(AddPhotoImageTitleBlackColor)];
        [lblImageUrl setBackgroundColor:UIColorFromRGBF(AddPhotoImageURLBlackColor)];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
