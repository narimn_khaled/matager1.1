//
//  UserDataManager.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "UserDataManager.h"
#import "MessageDataModel.h"


@interface UserDataManager ()

-(void)getUserInbox:(void(^)(NSMutableArray * Messages))inboxMessages;
-(void)getUserOutBox:(void(^)(NSMutableArray * Messages))outboxMessages;

@end


@implementation UserDataManager
@synthesize delegate;

+(UserDataManager *)SharedInstance{
    static UserDataManager * _sharedInstance=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance =[[UserDataManager alloc]init];
        [_sharedInstance logUserIn];
    });
    return _sharedInstance;
}

-(void)logUserIn{
    NSDictionary * userStoredInfo=[[NSUserDefaults standardUserDefaults]objectForKey:@"userInfo"];
    if (userStoredInfo != nil) {
        [self logUserInWithUserName:[userStoredInfo objectForKey:@"name"] andPassword:[userStoredInfo objectForKey:@"pass"]];
    }else{
        user =[[UserModel alloc]init];
    }
   
}

-(BOOL)logUserInWithUserName:(NSString *)userName andPassword:(NSString *)password{
    user =[[UserModel alloc]initLoggedUserWithUserID:@"1" withName:@"Ahmed" andPassword:@"123"];
    [[NSUserDefaults standardUserDefaults]setObject:[user userModelLoginDictionary] forKey:@"userInfo"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if ([delegate respondsToSelector:@selector(UserDataManagerUserLoginStatusChanged)]) {
        [delegate UserDataManagerUserLoginStatusChanged];
    }
    
    return YES;
}

-(BOOL)isUserLogged{
    return user.islogged;
}
-(BOOL)isCurrentLoggedUser:(NSString *)userID{
    if ([user.userID isEqualToString:userID]) {
        return user.islogged;
    }
    return NO;
}

#pragma -mark messages 

-(void)getUserMessagesForInbox:(BOOL)isInbox WithMessages:(void (^)(NSMutableArray *))Messages{
    if (isInbox) {
        [self getUserInbox:Messages];
    }else{
        [self getUserOutBox:Messages];
    }
}

-(void)getUserInbox:(void (^)(NSMutableArray *))inboxMessages{
    NSMutableArray * inbox=[[NSMutableArray alloc]init];
    for (int i=0; i<5; i++) {
        // don't forget to send type bool with the dictionary
        MessageDataModel * message =[[MessageDataModel alloc ]init];
        [message setMessageTypeInbox:YES];
        [inbox addObject:message];
    }
    inboxMessages(inbox);
}

-(void)getUserOutBox:(void (^)(NSMutableArray *))outboxMessages{
    NSMutableArray * outbox=[[NSMutableArray alloc]init];
    for (int i=0; i<5; i++) {
        // don't forget to send type bool with the dictionary
        MessageDataModel * message =[[MessageDataModel alloc ]init];
        [message setMessageTypeInbox:NO];
        [outbox addObject:message];
    }
    outboxMessages(outbox);

}

-(void)deleteUserMessage:(MessageDataModel *)message WithSuccess:(void (^) (BOOL doneDeleting))deleted{
    [[[UIAlertView alloc]initWithTitle:@"test" message:@"message deleted" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles: nil]show];
    deleted(YES);
}

@end
