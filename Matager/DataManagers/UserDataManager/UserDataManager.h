//
//  UserDataManager.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ShopModel.h"
#import "UserModel.h"
#import "MessageDataModel.h"

@protocol UserDataManagerDelegate <NSObject>

-(void)UserDataManagerUserLoginStatusChanged;

@end


@interface UserDataManager : NSObject{
    UserModel * user;
    
}


@property (nonatomic , assign) id<UserDataManagerDelegate> delegate;


#pragma -mark Class Methods
+(UserDataManager *)SharedInstance;

#pragma -mark object methods

#pragma -mark login/out 
-(void)logUserIn;
-(BOOL)logUserInWithUserName:(NSString *)userName andPassword:(NSString *)password;
-(BOOL)isUserLogged;
-(BOOL)isCurrentLoggedUser:(NSString *)userID;


#pragma -mark userData  / products / shops 
-(UserModel*)getUserData;
-(NSMutableArray *)getUserAds;
-(ShopModel *)getUserShopData;




#pragma -mark Messages 
-(void)getUserMessagesForInbox:(BOOL)isInbox WithMessages:(void (^) (NSMutableArray * messages))Messages;
-(void)deleteUserMessage:(MessageDataModel *)message WithSuccess:(void (^) (BOOL doneDeleting))deleted;

@end
