//
//  ProductsDataManager.h
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductModel.h"

@interface ProductsDataManager : NSObject

+(void)getProductsOfType:(enum homeProductType)type WithResultedProducts:(void (^) (NSMutableArray * productsMArray))products;

+(void)getCityProductsWithCityID:(NSString *)cityID andProducts:(void (^) (NSMutableArray * productsMArray))newProducts;

+(NSString *)getProductsSavedCountryID;



+(void)getProductDetailsWithProductID:(NSString *)ID withProduct:(void (^) (ProductModel * product))returnProduct;
+(void)getProductCommentsWithProductID:(NSString *)ID withProductComments:(void (^) (NSMutableArray * productComments))returnComments;

@end
