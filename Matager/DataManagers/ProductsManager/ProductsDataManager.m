//
//  ProductsDataManager.m
//  Matager
//
//  Created by Ali Amin on 11/12/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#import "ProductsDataManager.h"
#import "CommentDataModel.h"

@implementation ProductsDataManager

#pragma -mark home view functions 

+(void)getProductsOfType:(enum homeProductType)type WithResultedProducts:(void (^)(NSMutableArray *))products{
    if (type==newProducts) {
        [self getNewProducts:products];
    }else if (type==noPicProducts){
        [self getNoImagesProducts:products];
    }
}

+(void)getNewProducts:(void (^)(NSMutableArray *))newProducts{
    NSMutableArray * productsMArray=[NSMutableArray new];
    for (int i =0; i<5; i++) {
        ProductModel * product=[ProductModel new];
        [productsMArray addObject:product];
    }
    newProducts(productsMArray);
}
+(void)getNoImagesProducts:(void (^)(NSMutableArray *))newProducts{
    NSMutableArray * productsMArray=[NSMutableArray new];
    for (int i =0; i<5; i++) {
        ProductModel * product=[ProductModel new];
        [productsMArray addObject:product];
    }
    newProducts(productsMArray);
}

+(void)getCityProductsWithCityID:(NSString *)cityID andProducts:(void (^)(NSMutableArray *))newProducts{
    NSMutableArray * productsMArray=[NSMutableArray new];
    for (int i =0; i<5; i++) {
        ProductModel * product=[ProductModel new];
        [productsMArray addObject:product];
    }
    newProducts(productsMArray);
}



+(NSString *)getProductsSavedCountryID{
    NSString * countryID=  [[NSUserDefaults standardUserDefaults]objectForKey:@"countryID"];
    NSLog(@"country ID : %@" , countryID);
    return countryID;
}


+(void)getProductDetailsWithProductID:(NSString *)ID withProduct:(void (^)(ProductModel *))returnProduct{
    
    ProductModel * idProduct=[[ProductModel alloc]init];
    returnProduct(idProduct);
}
+(void)getProductCommentsWithProductID:(NSString *)ID withProductComments:(void (^)(NSMutableArray *))returnComments{
    NSMutableArray * commentsMArray=[NSMutableArray new];
    for (int i =0; i<5; i++) {
        CommentDataModel * comment=[CommentDataModel new];
        [commentsMArray addObject:comment];
    }
    returnComments(commentsMArray);
}










@end










