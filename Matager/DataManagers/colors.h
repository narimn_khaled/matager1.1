//
//  colors.h
//  Matager
//
//  Created by Ali Amin on 11/11/13.
//  Copyright (c) 2013 Artgine. All rights reserved.
//

#ifndef Matager_colors_h
#define Matager_colors_h

#define UIColorFromRGB(rgbValue)  colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0

#define UIColorFromRGBF(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define headerTitleColor 0xffffff
#define subHeaderTitleColor 0x439be8

#define sideMenuItemColor 0xffffff
#define sideMenuItemColor_s 0x88c8fb

#define blackListGrayColor 0xb4b3b4
#define blackListRedColor 0xff0006
#define blackListBlueColor 0x007ad7
#define blackListGreenColor 0x009b00


#define ProductCellTitleColor 0x404040
#define ProductCell3ardColor 0xf50000
#define ProductCellTalabColor 0x001bf8
#define ProductCellTextButtonsNormalColor 0x828282
#define ProductCellTextButtonsSelectedColor 0x439be8


#define messageBlackColor 0x434344
#define messageGrayColor 0x89898b


#define ProductDetailsBabyBlueColor 0x439be8
#define ProductDetailsLightGrayColor 0x89898b
#define ProductDetailsBlueColor 0x004ca8
#define ProductDetailsBlackColor 0x2c3136
#define ProductDetailsRedColor 0xab0807
#define ProductDetailsdescriptionColor 0x434344
#define ProductDetailsDark Gray: #666666

#define AddPhotoImageURLBlackColor 0x2c3136
#define AddPhotoImageTitleBlackColor 0x2c3136
#define AddAdAdTypeLabelLBlackColor 0x2c3136
#define AddAdAdTitlelabelBlackColor 0x2c3136
#define AddAdAdTitleTextBlackColor 0x2c3136
#define AddAdCommunicationLabelBlackColor 0x2c3136
#define AddAdCommunicationTextBlackColor 0x2c3136
#define AddAdCountryLabelBlackColor 0x2c3136
#define AddAdConutryTextBlackColor 0x2c3136
#define AddAdLocationLabelBlackColor 0x2c3136

#define AddAdManfLabelBlackColor 0x2c3136
#define AddAdModelLabelBlackColor 0x2c3136
#define AddAdYearLabelBlackColor 0x2c3136
#define AddAdContentLabelBlackColor 0x2c3136
#define AddAdContentTextBlackColor 0x2c3136
#define AddAdConfirmCodeLabelBlackColor 0x2c3136
#define AddAdConfirmCodeTextBlackColor 0x2c3136

#endif
